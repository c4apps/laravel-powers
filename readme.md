# Laravel Powers

A package for handling roles and permissions like a superhero. Made with love by C4 Tech and Design.

[![Latest Stable Version](https://poser.pugx.org/c4tech/powers/v/stable)](https://packagist.org/packages/c4tech/powers)
[![Build Status](https://travis-ci.org/C4Tech/laravel-powers.svg?branch=master)](https://travis-ci.org/C4Tech/laravel-powers)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/g/C4Tech/laravel-powers/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/g/C4Tech/laravel-powers/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/g/C4Tech/laravel-powers/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/C4Tech/laravel-powers/?branch=master)

## Powers

We've dealt with user permissions so many times in Laravel and never could
find a package that did everything we wanted. So we put on spandex (no capes!) and made our own to handle permissions with contextual scopes. This package can
handle both explicit and implicit privileges.

### Explicit Privileges

Explicit Privileges are permitted to individually scoped objects (e.g.
`\App\Post` object id 412), following the standard polymorphic relations of
Laravel on either the Power-Privilege relation or the Powerable-Power
relation. For example, a "Blog Authors" Power may be given "read" Privilege to
a private Page for author instructions (that is, the Power-Privilege pivot
table has the `scopable` morph set to `\App\Page` object 23). This would mean
that every Powerable assigned the "Blog Authors" power would get that read
permission. However, Powers can also be given unscoped Privileges (e.g. the
same "read" Privilege is associated with the Power but with the Power-
Privelege pivot table has the `scopable` morph set to null) which receive
their scope from the Power assignment (i.e. the Powerables-Power pivot table
row has the `scopable` morph set to the specific `App\Blog` object).

Does that sound confusing? Here's a short list showing the five possible cases
in which explicit permissions work:

* `$user->may($create, $page);` = true (Both Power-Privilege
    `scopable` for and Powerable-Power `scopable` are null). This is the
    classic, scopeless, role-and-permission check.
* `$user->may($read, $instructions_page);` = true (Power-Privilege
    `scopable` matches $instructions_page, Powerable-Power `scopable` is
    either null or matches $instructions_page).
* `$user->may($read, $page);` = false (Neither the Power-Privilege `scopable`
    nor the Powerable-Power `scopable` match $page, and both `scopable`s are
    not null).
* `$user->may($read, $own_blog);` = true (The Powerable-Power `scopable`
    matches $own_blog and the Power-Privilege `scopable` is either null or
    matches $own_blog).
* `$user->may($read, $other_blog);` = false (Neither the Power-Privilege
    `scopable` nor the Powerable-Power `scopable` match $other_blog, and both
    `scopable`s are not null).


### Implicit Privileges

In addition to explicit Privileges, we provide a flexible way to encourage
implicit privileges (e.g. delete Comments associated with my blog Post) rather
than fill the database with explicit Privileges. Implicit privileges are
triggered after explicit Permissions return false, and use mixed `scopable`
values. Instead of having the `scopable` morph match the object precisely,
the `scopable_id` is set to null while the `scopable_type` matches the class
of the resource scope. However, the scope object must implement an additional
`ImplicitPrivilegeInterface`. This interface provides a callback which will
receive the Powerable, Privilege, the Privilege resource scope, and the Role resource scope. From there, you (the developer) can include your own logic. For
the example above, it would be something like:

1. The "Post Author" Power would be associated with a Powerable where
    `scopable_type` set to `App\Post` but `scopable_id` set to `NULL` and the
    "delete" Privilege would be associated with the Power where
    `scopable_type` set to `App\Comment` but `scopable_id` set to `NULL`. And..

2. `App\Comment` implements `ImplicitPrivilegeInterface`:
```
public function resolvePrivilege(
    PowerableInterface $powerable,
    Privilege Interface $privilege,
    ScopableInterface $power_scope = null
) {
    if ($privilege->name === 'delete'
        && $this->post_id === $power_scope->id
        && $power_scope->owner === $powerable->id
    ) {
        return true;
    }
}
```


## Installation and setup

1. Add `"c4tech/powers": "1.x"` to your composer requirements and run `composer update`.
2. Add `C4tech\Powers\ServiceProvider` to `config/app.php` in the 'providers' array.
3. `php artisan vendor:publish`
4. Adjust `config/powers.php` (or `config/foundation.php` if you're using `c4tech/foundation`) to bind your extended models and repos to the facades.
