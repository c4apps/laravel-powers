<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerPrivilegeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('power_privilege', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('power_id')->index();
            $table->unsignedInteger('privilege_id')->index();
            $table->string('scopable_type')->nullable();
            $table->unsignedInteger('scopable_id')->nullable();
            $table->timestamps();

            $table->index(['scopable_id', 'scopable_type']);
            $table->unique(['power_id', 'privilege_id', 'scopable_id', 'scopable_type'], 'responsibility');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('power_privilege');
    }
}
