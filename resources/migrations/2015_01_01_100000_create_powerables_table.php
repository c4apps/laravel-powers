<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePowerablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('powerables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('power_id')->index();
            $table->morphs('powerable');
            $table->string('scopable_type')->nullable();
            $table->unsignedInteger('scopable_id')->nullable();
            $table->timestamps();

            $table->index(['scopable_id', 'scopable_type']);
            $table->unique(
                [
                    'power_id',
                    'powerable_id',
                    'powerable_type',
                    'scopable_id',
                    'scopable_type'
                ],
                'assignment'
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('powerables');
    }
}
