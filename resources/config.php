<?php

return [
    'models' => [
        'power' => 'C4tech\Powers\Power\Model',
        'privilege' => 'C4tech\Powers\Privilege\Model'
    ],

    'repos' => [
        'power' => 'C4tech\Powers\Power\Repository',
        'privilege' => 'C4tech\Powers\Privilege\Repository'
    ]
];
