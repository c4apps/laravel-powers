<?php namespace C4tech\Powers\Powerable;

use C4tech\Powers\Contracts\PrivilegeInterface;
use C4tech\Powers\Contracts\ScopableInterface;
use C4tech\Powers\Contracts\PowerInterface;
use Illuminate\Support\Facades\DB;

trait RepositoryTrait
{
    /**
     * [$privilege_relation description]
     * @var string
     */
    protected $privilege_relation = 'privileges';

    /**
     * [$privilege_pivot description]
     * @var string
     */
    protected $privilege_pivot = 'power_privilege';

    /**
     * [$power_pivot description]
     * @var string
     */
    protected $power_pivot = 'powerables';

    /**
     * [$scope_morph description]
     * @var string
     */
    protected $scope_morph = 'scopable';

    /**
     * @inheritDoc
     */
    public function may(
        PrivilegeInterface $privilege,
        ScopableInterface $privilege_scope = null,
        ScopableInterface $power_scope = null
    ) {
        $query = $this->powers();

        $query->whereHas($this->privilege_relation, function ($relation) use ($privilege, $privilege_scope) {
            $relation->where(
                $this->privilege_pivot . '.' . $privilege->model->getForeignKey(),
                '=',
                $privilege->model->getKey()
            );

            $relation->where($this->scopeSubquery($privilege_scope));

            return $relation;
        });

        if ($privilege_scope && !$power_scope) {
            $power_scope = $privilege_scope;
        }

        if ($power_scope) {
            $query->where(function ($builder) use ($power_scope) {
                return $builder->where($this->scopeSubquery($power_scope, $this->power_pivot));
            });
        }

        $count = $query->count();
        if ($count) {
            return true;
        } elseif ($privilege_scope instanceof ImplicitPrivilegeInterface) {
            if ($privilege_scope->resolvePrivilege($this, $privilege, $power_scope)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function exertsOn(
        ScopableInterface $privilege_scope,
        ScopableInterface $power_scope = null
    ) {
        $query = $this->powers();

        $query->whereHas($this->privilege_relation, function ($relation) use ($privilege_scope) {
            return $relation->where($this->scopeSubquery($privilege_scope));
        });

        if (!$power_scope) {
            $power_scope = $privilege_scope;
        }

        $query->where(function ($builder) use ($power_scope) {
            return $builder->where($this->scopeSubquery($power_scope, $this->power_pivot));
        });

        if ($powers = $query->get()) {
            $powers->map();
        }
    }

    /**
     * @inheritDoc
     */
    public function bestow(PowerInterface $power, ScopableInterface $power_scope = null)
    {
        $pivot_data = [];
        if ($power_scope) {
            $pivot_data[$this->scope_morph . '_type'] = $power_scope->getModelClass();

            if ($power_scope->exists) {
                $pivot_data[$this->scope_morph . '_id'] = $power_scope->model->getKey();
            }
        }

        $result = $this->object->powers()->attach($power->model, $pivot_data);
        $power->model->touch();

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function withhold(PowerInterface $power, ScopableInterface $power_scope = null)
    {
        $query = $this->object->powers()
            ->newPivotStatementForId($power->model->getKey());

        if ($power_scope) {
            $query->where($this->scope_morph . '_type', $power_scope->getModelClass());
            $power_key = $this->scope_morph . '_id';

            if ($power_scope->exists) {
                $query->where($power_key, $power_scope->model->getKey());
            } else {
                $query->whereNull($power_key);
            }
        } else {
            $query->whereNull($this->scope_morph . '_type');
        }

        $results = $query->delete();
        $power->model->touch();

        return $results;
    }

    protected function powers()
    {
        return $this->object->powers();
    }

    /**
     * Scope Subquery
     *
     * Generate a query closure to check if scopable morph is null or matches given
     * scope.
     * @param  ScopableModelInterface $scope The morphable scope to query.
     * @param  string                 $table The table name.
     * @return callable
     */
    private function scopeSubquery(ScopableInterface $scope = null, $table = null)
    {
        if (is_null($table)) {
            $table = $this->privilege_pivot;
        }

        $null_query = function ($subquery) use ($table) {
            return $subquery->whereNull($table . '.' . $this->scope_morph . '_type')
                ->whereNull($table . '.' . $this->scope_morph . '_id');
        };

        if (is_null($scope)) {
            return $null_query;
        }

        return function ($query) use ($scope, $table, $null_query) {
            $query->where(function ($subquery) use ($scope, $table) {
                $subquery->where($table . '.' . $this->scope_morph . '_type', $scope->getModelClass());

                if ($scope->model->getKey() > 0) {
                    $subquery->where(function ($sql) use ($scope, $table) {
                        return $sql->where($table . '.' . $this->scope_morph . '_id', $scope->model->getKey())
                            ->orWhereNull($table . '.' . $this->scope_morph . '_id');
                    });
                }
            })
            ->orWhere($null_query);
        };
    }
}
