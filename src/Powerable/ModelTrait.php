<?php namespace C4tech\Powers\Powerable;

use C4tech\Powers\Power\Facade as Power;

trait ModelTrait
{
    /**
     * @inheritDoc
     */
    public function powers()
    {
        return $this->morphToMany(Power::getModelClass(), 'powerable');
    }
}
