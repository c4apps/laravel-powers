<?php namespace C4tech\Powers\Contracts;

use C4tech\Support\Contracts\ModelInterface;

interface PowerModelInterface extends ModelInterface
{
    /**
     * Powerables
     *
     * Query for Powerables with ths Power, optionally limited by type.
     * @param  string $type Class name.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function powerables($type);

    /**
     * Privileges
     *
     * Query for Privileges supplied by this Power, optionally scoped to a resource.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function privileges();
}
