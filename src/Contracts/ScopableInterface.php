<?php namespace C4tech\Powers\Contracts;

interface ScopableInterface
{
    /**
     * Get Cache Suffix
     *
     * Retrieve a unique cache suffix for caching through related repositories.
     * @return string
     */
    public function getCacheSuffix();
}
