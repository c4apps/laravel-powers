<?php namespace C4tech\Powers\Contracts;

interface ImplicitPrivilegeInterface
{
    /**
     * Resolve Privilege
     *
     * Provides context-specific implied privileges.
     * @param  PowerableInterface $powerable   The Powerable to authorize.
     * @param  PrivilegeInterface $privilege   The Privilege to check.
     * @param  ScopableInterface  $power_scope Optional secondary scope context.
     * @return boolean|void
     */
    public function resolvePrivilege(
        PowerableInterface $powerable,
        PrivilegeInterface $privilege,
        ScopableInterface $power_scope = null
    );
}
