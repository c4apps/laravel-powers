<?php namespace C4tech\Powers\Contracts;

interface PowerableInterface
{
    /**
     * May
     *
     * May this Powerable perform the given Privilege, optionally scoped to resources?
     *
     * @param  PrivilegeInterface $privilege       The Privilege to check.
     * @param  ScopableInterface  $privilege_scope Optional scope context.
     * @param  ScopableInterface  $power_scope     Optional secondary scope context.
     * @return boolean
     */
    public function may(
        PrivilegeInterface $privilege,
        ScopableInterface $privilege_scope = null,
        ScopableInterface $power_scope = null
    );

    /**
     * Exerts On
     *
     * What Privileges may be performed on the scoped resource?
     *
     * @param  ScopableInterface $privilege_scope  Scope context.
     * @param  ScopableInterface $power_scope      Secondary scope context.
     * @return \Illuminate\Support\Collection
     */
    public function exertsOn(
        ScopableInterface $privilege_scope,
        ScopableInterface $power_scope = null
    );

    /**
     * Bestow
     *
     * Grant a Power to a Powerable, optionally providing a resource scope.
     * @param  PowerInterface    $power       The Power to grant.
     * @param  ScopableInterface $power_scope Optional scope for the Power.
     * @return boolean
     */
    public function bestow(
        PowerInterface $power,
        ScopableInterface $power_scope = null
    );

    /**
     * Withhold
     *
     * Revoke a Power from a Powerable, optionally providing a resource scope.
     * @param  PowerInterface    $power       The Power to revoke.
     * @param  ScopableInterface $power_scope Optional scope for the Power.
     * @return boolean
     */
    public function withhold(
        PowerInterface $power,
        ScopableInterface $power_scope = null
    );
}
