<?php namespace C4tech\Powers\Contracts;

interface PowerableModelInterface
{
    /**
     * Powers
     *
     * Query for Powers possessed by this Powerable, optionally limited by Privilege
     * name and/or scoped to a resource.
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function powers();
}
