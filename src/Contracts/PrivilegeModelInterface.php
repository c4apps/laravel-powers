<?php namespace C4tech\Powers\Contracts;

use C4tech\Support\Contracts\ModelInterface;

interface PrivilegeModelInterface extends ModelInterface
{
    /**
     * Powers
     *
     * Query for Powers that supply this Privilege, optionally scoped to a resource.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function powers();
}
