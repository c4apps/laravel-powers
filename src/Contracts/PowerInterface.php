<?php namespace C4tech\Powers\Contracts;

use C4tech\Support\Contracts\ResourceInterface;

interface PowerInterface extends ResourceInterface
{
    /**
     * Find By Name
     *
     * Retrieve Power by name.
     * @param  string $name Role name
     * @return static
     */
    public function findByName($name);

    /**
     * Get Powerables
     *
     * Retrieve all Powerables with ths Power, limited by morphed class name.
     * @param  string $type            Class name.
     * @param  string $repository_type Repository name.
     * @return \Illuminate\Support\Collection
     */
    public function getPowerables($type, $repository_type);

    /**
     * Get Privileges
     *
     * Retrieve all Privileges which are supplied this Power, optionally scoped to a resource.
     * Will be cached as well.
     * @param ScopableInterface $scope Optional scope context.
     * @return \Illuminate\Support\Collection
     */
    public function getPrivileges(ScopableInterface $scope = null);
}
