<?php namespace C4tech\Powers\Contracts;

use C4tech\Support\Contracts\ResourceInterface;

interface PrivilegeInterface extends ResourceInterface
{
    /**
     * Find By Name
     *
     * Retrieve privilee by a given name.
     * @param  string $name Privilege name
     * @return static
     */
    public function findByName($name);

    /**
     * Get Powers
     *
     * Retrieve all Powers which supply this Privilege, optionally scoped to a resource.
     * Will be cached as well.
     * @param ScopableInterface $scope Optional scope context.
     * @return \Illuminate\Support\Collection
     */
    public function getPowers(ScopableInterface $scope = null);
}
