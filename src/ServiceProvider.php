<?php namespace C4tech\Powers;

use C4tech\Powers\Power\Facade as Power;
use C4tech\Powers\Privilege\Facade as Privilege;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    protected $configPath = '';

    protected $migrationPath = '';

    /**
     * @inheritDoc
     */
    public function __construct($app)
    {
        $this->configPath = __DIR__ . '/../resources/config.php';
        $this->migrationPath = __DIR__ . '/../resources/migrations';
        parent::__construct($app);
    }

    /**
     * @inheritDoc
     */
    public function boot()
    {
        $configs = [];
        $configs[$this->configPath] = config_path('powers.php');
        $this->publishes($configs, 'config');

        $migrations = [];
        $migrations[$this->migrationPath] = database_path('migrations');
        $this->publishes($migrations, 'migrations');

        Power::boot();
        Privilege::boot();
    }

    /**
     * @inheritDoc
     */
    public function register()
    {
        $this->mergeConfigFrom($this->configPath, 'powers');

        App::singleton(
            'c4tech.power',
            function () {
                $repo = Config::get('powers.repos.power', 'C4tech\Powers\Power\Repository');
                $repo = Config::get('foundation.repos.power', $repo);
                return new $repo;
            }
        );

        App::singleton(
            'c4tech.privilege',
            function () {
                $repo = Config::get('powers.repos.privilege', 'C4tech\Powers\Privilege\Repository');
                $repo = Config::get('foundation.repos.privilege', $repo);
                return new $repo;
            }
        );
    }

    /**
     * @inheritDoc
     */
    public function provides()
    {
        return ['c4tech.power', 'c4tech.privilege'];
    }
}
