<?php namespace C4tech\Powers\Power;

use C4tech\Powers\Contracts\PowerModelInterface;
use C4tech\Powers\Contracts\ScopableModelInterface;
use C4tech\Powers\Privilege\Facade as Privilege;
use C4tech\Support\Model as BaseModel;

class Model extends BaseModel implements PowerModelInterface
{
    /**
     * @inheritDoc
     */
    protected $table = 'powers';

    /**
     * @inheritdoc
     */
    public function getForeignKey()
    {
        return 'power_id';
    }

    /**
     * Scope: Where Name
     * @param  \Illuminate\Database\Eloquent\Builder $query Query Builder
     * @param  string                                $name  Name to find
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereName($query, $name)
    {
        return $query->where('name', $name);
    }

    /**
     * @inheritDoc
     */
    public function powerables($type)
    {
        return $this->morphedByMany($type, 'powerable');
    }

    /**
     * @inheritDoc
     */
    public function privileges()
    {
        return $this->belongsToMany(Privilege::getModelClass(), 'power_privilege')
            ->withPivot('scopable_type', 'scopable_id')
            ->withTimestamps();
    }
}
