<?php namespace C4tech\Powers\Power;

use C4tech\Powers\Contracts\PowerInterface;
use C4tech\Powers\Contracts\ScopableInterface;
use C4tech\Powers\Contracts\PrivilegeInterface;
use C4tech\Powers\Privilege\Facade as Privilege;
use C4tech\Support\Repository as BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class Repository extends BaseRepository implements PowerInterface
{
    /**
     * @inheritDoc
     */
    protected static $model = '.models.power';

    /**
     * @inheritDoc
     */
    public function getModelClass()
    {
        $class = Config::get('powers' . static::$model, 'powers' . static::$model);
        return Config::get('foundation' . static::$model, $class);
    }

    /**
     * @inheritDoc
     */
    public function findByName($name)
    {
        return Cache::tags($this->getTags('power-name-' . $name))
            ->remember(
                $this->getCacheId('power-name-' . $name),
                self::CACHE_MONTH,
                function () use ($name) {
                    if ($object = $this->object->whereName($name)->first()) {
                        return $this->make($object);
                    }
                }
            );
    }

    /**
     * Privileges
     *
     * Wrapper method to underlying model query method.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function privileges()
    {
        return $this->object->privileges();
    }

    /**
     * @inheritDoc
     */
    public function getPrivileges(ScopableInterface $scope = null)
    {
        $suffix = (is_null($scope)) ? '' : '-' . $scope->getCacheSuffix();
        return Cache::tags($this->getTags('privileges'))
            ->remember(
                $this->getCacheId('privileges' . $suffix),
                self::CACHE_MONTH,
                function () use ($scope) {
                    $query = $this->privileges();

                    if (!is_null($scope)) {
                        $query->wherePivot(function ($pivot) use ($scope) {
                            return $pivot->where('scopable_type', $scope->getModelClass())
                                ->where('scopable_id', $scope->getKey());
                        });
                    }

                    $privileges = $query->get();
                    return Privilege::makeCollection($privileges);
                }
            );
    }

    public function addPrivilege(PrivilegeInterface $privilege, ScopableInterface $scope = null)
    {
        $pivot_data = [];
        if ($scope) {
            $pivot_data['scopable_type'] = $scope->getModelClass();

            if ($scope->id) {
                $pivot_data['scopable_id'] = $scope->id;
            }
        }

        $this->privileges()->attach($privilege->id, $pivot_data);
    }

    /**
     * Powerables
     *
     * Wrapper method to underlying model query method.
     * @param  string $type Class name.
     * @return \Illuminate\Database\Eloquent\Relations\MorphsToMany
     */
    protected function powerables($type)
    {
        return $this->object->powerables($type);
    }

    /**
     * @inheritDoc
     */
    public function getPowerables($type, $facade)
    {
        return Cache::tags($this->getTags($type))
            ->remember(
                $this->getCacheId('powerables-' . $type),
                self::CACHE_WEEK,
                function () use ($type, $facade) {
                    $heros = $this->powerables($type)->get();
                    return $facade::makeCollection($heros);
                }
            );
    }
}
