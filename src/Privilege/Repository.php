<?php namespace C4tech\Powers\Privilege;

use C4tech\Powers\Contracts\PrivilegeInterface;
use C4tech\Powers\Contracts\ScopableInterface;
use C4tech\Powers\Power\Facade as Power;
use C4tech\Support\Repository as BaseRepository;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class Repository extends BaseRepository implements PrivilegeInterface
{
    /**
     * @inheritDoc
     */
    protected static $model = '.models.privilege';

    /**
     * @inheritDoc
     */
    public function getModelClass()
    {
        $class = Config::get('powers' . static::$model, 'powers' . static::$model);
        return Config::get('foundation' . static::$model, $class);
    }

    /**
     * @inheritDoc
     */
    public function findByName($name)
    {
        return Cache::tags($this->getTags('privilege-name-' . $name))
            ->remember(
                $this->getCacheId('privilege-name-' . $name),
                self::CACHE_MONTH,
                function () use ($name) {
                    if ($object = $this->object->whereName($name)->first()) {
                        return $this->make($object);
                    }
                }
            );
    }

    /**
     * Powers
     *
     * Wrapper method to underlying model query method.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    protected function powers()
    {
        return $this->object->powers();
    }

    /**
     * @inheritDoc
     */
    public function getPowers(ScopableInterface $scope = null)
    {
        $suffix = (is_null($scope)) ? '' : '-' . $scope->getCacheSuffix();
        return Cache::tags($this->getTags('powers'))
            ->remember(
                $this->getCacheId('powers' . $suffix),
                self::CACHE_MONTH,
                function () {
                    $query = $this->powers();

                    if (!is_null($scope)) {
                        $query->wherePivot(function ($query) use ($scope) {
                            return $query->where('scopable_type', $scope->getModelClass())
                                ->where('scopable_id', $scope->getKey());
                        });
                    }

                    $powers = $query->get();

                    return Power::makeCollection($powers);
                }
            );
    }
}
